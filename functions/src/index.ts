import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
admin.initializeApp();
const stripe = require('stripe')(functions.config().stripe.token);
const currency = functions.config().stripe.currency || 'usd';

exports.fetchStripeUserAndSave = functions.https.onRequest((request, response) => {
	response.set('Access-Control-Allow-Origin', '*');
	response.set('Access-Control-Allow-Methods', 'GET, PUT, POST, OPTIONS');
	response.set('Access-Control-Allow-Headers', '*');

	if (request.method === 'OPTIONS') {
	   response.end();
	}

	else{
		const authorization_code = request.query.authorization_code;
		const entityId = request.query.entityId;
		return stripe.oauth.token({
			grant_type: 'authorization_code',
			code: authorization_code
		}).then((res: any) => {
			console.log(res);
			return admin.firestore().collection('partners').doc(entityId).update({stripe_info: res}).then(() => {
					response.send({status: 'success', message: 'Stripe account is connected successfully.'});				
				})
		}).catch((err: any) => {
			response.send({status: 'failure', message: 'We cannot connect your Stripe account.'});
		})
	}
})

exports.revokeStripeUser = functions.https.onRequest((request, response) => {
	response.set('Access-Control-Allow-Origin', '*');
	response.set('Access-Control-Allow-Methods', 'GET, PUT, POST, OPTIONS');
	response.set('Access-Control-Allow-Headers', '*');

	if (request.method === 'OPTIONS') {
	   response.end();
	}
	else {
		const client_id = request.query.client_id;
		const stripe_user_id = request.query.stripe_user_id;
		const entityId = request.query.entityId;
		stripe.oauth.deauthorize({
		  client_id: client_id,
		  stripe_user_id: stripe_user_id,
		}).then((res: any) => {
			console.log(res);
			return admin.firestore().collection('partners').doc(entityId).update({stripe_info: null}).then(() => {
					response.send({status: 'success',  message: 'Revoked.'})				
				})
		}).catch((err: any) => {
			console.log(err);
			response.send({status: 'failure', message: err.message})
		})

	}
})

exports.createCharge = functions.https.onRequest((request, response) => {
	response.set('Access-Control-Allow-Origin', '*');
	response.set('Access-Control-Allow-Methods', 'GET, PUT, POST, OPTIONS');
	response.set('Access-Control-Allow-Headers', '*');

	if (request.method === 'OPTIONS') {
	   response.end();
	}
	else {
		const amount = request.query.amount;
		const source = request.query.source;
		stripe.charges.create({
			amount: amount,
			currency: currency,
			source: source
		}).then((charge: any) => {
			console.log(charge);
			response.send({status:'success', message: charge})
		}).catch((err: any) => {
			console.log(err);
			let message = err.message;
			response.send({status:'failure', message: message})
		})
		
	}
})

exports.createTransfers = functions.https.onRequest((request, response) => {
	response.set('Access-Control-Allow-Origin', '*');
	response.set('Access-Control-Allow-Methods', 'GET, PUT, POST, OPTIONS');
	response.set('Access-Control-Allow-Headers', '*');

	if (request.method === 'OPTIONS') {
	   response.end();
	}
	else {
		const amount = request.query.amount;
		const description = request.query.description;
		const destination = request.query.destination;
		stripe.transfers.create({
			amount: amount,
			currency: currency,
			destination: destination,
			description: description
		}).then((transfer: any) => {
			response.send({status: 'success', message: transfer})
		}).catch((err: any) => {
			console.log(err)
			response.send({status: 'failure', message: err.message})
		})
	}

})

exports.createRefunds = functions.https.onRequest((request, response) => {
	response.set('Access-Control-Allow-Origin', '*');
	response.set('Access-Control-Allow-Methods', 'GET, PUT, POST, OPTIONS');
	response.set('Access-Control-Allow-Headers', '*');

	if (request.method === 'OPTIONS') {
	   response.end();
	}
	else {
		const charge = request.query.charge;
		const amount = request.query.amount;
		stripe.refunds.create({
			charge: charge,
			amount: amount
		}).then((refund: any) => {
			response.send({status: 'success', message: refund})
		}).catch((err: any) => {
			console.log(err)
			response.send({status: 'failure', message: err.message})
		})
	}
})

exports.createLoginLink = functions.https.onRequest((request, response) => {
	response.set('Access-Control-Allow-Origin', '*');
	response.set('Access-Control-Allow-Methods', 'GET, PUT, POST, OPTIONS');
	response.set('Access-Control-Allow-Headers', '*');

	if (request.method === 'OPTIONS') {
	   response.end();
	}
	else {		
		const stripe_user_id = request.query.stripe_user_id;
		stripe.accounts.createLoginLink(stripe_user_id)
		.then((link: any) => {
			response.send({status: 'success', message: link})
		})
		.catch((err: any) => {
			console.log(err)
		    response.send({status: 'failure', message: err.message})
		})
	}
})